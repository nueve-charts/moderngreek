{{/* vim: set filetype=mustache: */}}

{{/*
Expand the name of the chart.
*/}}
{{- define "moderngreek.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "moderngreek.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate api certificate
*/}}
{{- define "moderngreek.api-certificate" }}
{{- if (not (empty .Values.ingress.api.certificate)) }}
{{- printf .Values.ingress.api.certificate }}
{{- else }}
{{- printf "%s-api-letsencrypt" (include "moderngreek.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate dashboard certificate
*/}}
{{- define "moderngreek.dashboard-certificate" }}
{{- if (not (empty .Values.ingress.dashboard.certificate)) }}
{{- printf .Values.ingress.dashboard.certificate }}
{{- else }}
{{- printf "%s-dashboard-letsencrypt" (include "moderngreek.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate store certificate
*/}}
{{- define "moderngreek.store-certificate" }}
{{- if (not (empty .Values.ingress.store.certificate)) }}
{{- printf .Values.ingress.store.certificate }}
{{- else }}
{{- printf "%s-store-letsencrypt" (include "moderngreek.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate api hostname
*/}}
{{- define "moderngreek.api-hostname" }}
{{- if (and .Values.config.api.hostname (not (empty .Values.config.api.hostname))) }}
{{- printf .Values.config.api.hostname }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- printf .Values.ingress.api.hostname }}
{{- else }}
{{- printf "%s-moderngreek" (include "moderngreek.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate api base url
*/}}
{{- define "moderngreek.api-base-url" }}
{{- if (and .Values.config.api.baseUrl (not (empty .Values.config.api.baseUrl))) }}
{{- printf .Values.config.api.baseUrl }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- $hostname := ((empty (include "moderngreek.api-hostname" .)) | ternary .Values.ingress.api.hostname (include "moderngreek.api-hostname" .)) }}
{{- $protocol := (.Values.ingress.api.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "moderngreek.api-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate dashboard hostname
*/}}
{{- define "moderngreek.dashboard-hostname" }}
{{- if (and .Values.config.dashboard.hostname (not (empty .Values.config.dashboard.hostname))) }}
{{- printf .Values.config.dashboard.hostname }}
{{- else }}
{{- if .Values.ingress.dashboard.enabled }}
{{- printf .Values.ingress.dashboard.hostname }}
{{- else }}
{{- printf "%s-moderngreek" (include "moderngreek.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate dashboard base url
*/}}
{{- define "moderngreek.dashboard-base-url" }}
{{- if (and .Values.config.dashboard.baseUrl (not (empty .Values.config.dashboard.baseUrl))) }}
{{- printf .Values.config.dashboard.baseUrl }}
{{- else }}
{{- if .Values.ingress.dashboard.enabled }}
{{- $hostname := ((empty (include "moderngreek.dashboard-hostname" .)) | ternary .Values.ingress.dashboard.hostname (include "moderngreek.dashboard-hostname" .)) }}
{{- $protocol := (.Values.ingress.dashboard.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "moderngreek.dashboard-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate store hostname
*/}}
{{- define "moderngreek.store-hostname" }}
{{- if (and .Values.config.store.hostname (not (empty .Values.config.store.hostname))) }}
{{- printf .Values.config.store.hostname }}
{{- else }}
{{- if .Values.ingress.store.enabled }}
{{- printf .Values.ingress.store.hostname }}
{{- else }}
{{- printf "%s-moderngreek" (include "moderngreek.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate store base url
*/}}
{{- define "moderngreek.store-base-url" }}
{{- if (and .Values.config.store.baseUrl (not (empty .Values.config.store.baseUrl))) }}
{{- printf .Values.config.store.baseUrl }}
{{- else }}
{{- if .Values.ingress.store.enabled }}
{{- $hostname := ((empty (include "moderngreek.store-hostname" .)) | ternary .Values.ingress.store.hostname (include "moderngreek.store-hostname" .)) }}
{{- $protocol := (.Values.ingress.store.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "moderngreek.store-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate mongo url
*/}}
{{- define "moderngreek.mongo-url" }}
{{- $mongo := .Values.config.mongo }}
{{- if $mongo.url }}
{{- printf $mongo.url }}
{{- else }}
{{- $credentials := ((or (empty $mongo.username) (empty $mongo.password)) | ternary "" (printf "%s:%s@" $mongo.username $mongo.password)) }}
{{- printf "mongoql://%s%s:%s/%s" $credentials $mongo.host $mongo.port $mongo.database }}
{{- end }}
{{- end }}
